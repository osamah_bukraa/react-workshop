import React, { Component } from 'react';
import "./detailsPage.css";

class DetailsPage extends Component{
    render(){
        return (<div className="details">
            <dl>
                <dt>Name</dt><dd>{this.props.planet.name}</dd>
                <dt>Icon</dt><dd><img src={this.props.planet.icon} className="planet-icon" alt={this.props.planet.name + "_icon"}/></dd>
                <dt>Gravity</dt><dd>{this.props.planet.gravity}</dd>
            </dl>
            <br clear="left"/>
        </div>);
    }
}

export default DetailsPage