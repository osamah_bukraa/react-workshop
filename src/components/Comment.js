import React, { Component } from "react";

class Comment extends Component {

    _deleteComment = () => {
        this.props.deleteComment(this.props.comment.id);
    }

    render() {
        return (
            <div className="comment">
                <figure>
                    <img src={this.props.comment.image} alt={this.props.comment.alt} />
                </figure>                
                <div className="content"> 
                    <i className="fa fa-times delete-button" aria-hidden="true" onClick={this._deleteComment}></i>
                    <p className="name">{this.props.comment.name} zegt:</p>
                    <p className="text">{this.props.comment.text}</p>
                    <p className="timestamp">{this.props.comment.timestamp.toString()}</p>
                </div>
            </div>
        );
    }
}

export default Comment;