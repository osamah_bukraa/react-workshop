import React, { Component } from 'react';
import "./planetOverview.css";
import PlanetOverviewItem from "./PlanetOverviewItem";
import PlanetForm from "./PlanetForm.js";
import Modal from 'react-modal';
import PlanetEditForm from "./PlanetEditForm.js";


class PlanetOverview extends Component {
    constructor(props){
        super(props);
        this.state = {
            planetFormShown : false,
            planetEditFormShown: false,
            currentlyEditedPlanet: undefined,
        }
    }
    _showAddPlanetForm = () => {
        this.setState({
            planetFormShown: true,
        })
    }
    _addPlanet = (planet) => {
        this.setState({
            planetFormShown: false,
        })
        this.props.addPlanet(planet);
    }
    _showEditPlanetForm = (planet) => {
        this.setState({
            planetEditFormShown: true,
            currentlyEditedPlanet: planet,
        });
    }
    _editPlanet = (planet) => {
        this.props.editPlanet(planet);
        this.setState({
            planetEditFormShown: false,
        });
    }
    _cancelAddPlanet = (ev) => {
        this.setState({
            planetFormShown: false,
        });
        ev.preventDefault();
    }
    _cancelEditPlanet = (ev) => {
        this.setState({
            planetEditFormShown: false,
        });
        ev.preventDefault();
    }
    render(){
        return (<div>
                <div className="overview">
                {this.props.planets.map(
                    planet => <PlanetOverviewItem 
                        selected={this.props.selectedPlanet && planet.id === this.props.selectedPlanet.id} 
                        key={planet.id} 
                        planet={planet} 
                        handleClick={this.props.changePlanet}
                        deletePlanet={this.props.deletePlanet}
                        editPlanet={this._showEditPlanetForm}
                    />
                )}
                <button onClick={this._showAddPlanetForm} className="add-planet-button">Add planet</button>
            </div>
            <Modal isOpen={this.state.planetFormShown}>
                <PlanetForm addPlanet={this._addPlanet} cancel={this._cancelAddPlanet}/>
            </Modal>
            <Modal isOpen={this.state.planetEditFormShown}>
                {this.state.currentlyEditedPlanet && <PlanetEditForm editPlanet={this._editPlanet} cancel={this._cancelEditPlanet} planet={this.state.currentlyEditedPlanet}/>}
            </Modal>
        </div>)
    }
}

export default PlanetOverview