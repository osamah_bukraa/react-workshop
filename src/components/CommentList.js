import React, {Component} from 'react';

import Comment from './Comment';
import AddComment from './AddComment';

import './CommentList.css';

class CommentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: []
        }
    }

    componentWillMount() {
        this._getData();        
    }

    componentWillReceiveProps(nextProps) {
        this._getData(nextProps);
    }

    _getData(nextProps = null) {
        let props = nextProps == null ? this.props : nextProps;
        fetch(`http://localhost:3000/comments?planetId=${props.planetId}`).then(
            res => {
                res.json().then(comments => {
                    this.setState({
                        comments: comments
                    });
                });
            }
        );
    }

    _addComment = (planetId, name, comment) => {
        fetch('http://localhost:3000/comments', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: new Date().getTime(),
                planetId: planetId,
                image: 'assets/logo.svg',
                alt: 'De afbeelding van ' + name,
                name: name,
                text: comment,
                timestamp: new Date().toLocaleDateString()
            })
        }).then(() => {
            this._getData(this.props);
        }); 
    }

    _deleteComment = (id) => {
        fetch(`http://localhost:3000/comments/${id}`, {
            method: 'DELETE'
        }).then(() => {
            this._getData(this.props);
        }); 
    }

    render() {
        return (
            <div>
                <div className="comments">
                    {this.state.comments.map(comment => {
                        return <Comment key={comment.id} comment={comment} deleteComment={this._deleteComment} />
                    })}
                </div>                
                <AddComment handleAddComment={this.props.handleAddComment} planetId={this.props.planetId} addComment={this._addComment} />
            </div>
        );
    }
}

export default CommentList;