import React, { Component } from 'react';

class AddComment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            comment: ''
        };
    }

    _addComment = () => {
        let name = this.state.userName;
        let comment = this.state.comment;
        let planetId = this.props.planetId;
        this.props.addComment(planetId, name, comment);
        this.setState({
            userName: '',
            comment: ''
        })         
    }

    _handleChangeName = (ev) => {
        this.setState({
          userName: ev.target.value
        })
      }

    _handleChangeText = (ev) => {
        this.setState({
          comment: ev.target.value
        })
    }

    render() {
        return(
            <div className='container'>
                <div className='add-comment'>
                    <h3>Zeg iets over deze planeet </h3>
                    <hr />
                    <div className="form-group">
                        <label>Naam:</label>
                        <input type='text' value={this.state.userName} onChange={this._handleChangeName} />
                    </div>
                    <div className="form-group">
                        <label>Commentaar:</label>
                        <input type='text' value={this.state.comment} onChange={this._handleChangeText} />
                    </div>
                    <button onClick={this._addComment}>Toevoegen</button>
                </div>
            </div>
        )
    }
}

export default AddComment;