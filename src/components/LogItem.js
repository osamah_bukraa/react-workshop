import React, {Component} from 'react';
import dateformat from 'dateformat';

class LogItem extends Component {

  constructor(props) {
    super(props);
    console.log(this.props.logitem.userimage);
  }

  _deleteLog = () => {
    this
      .props
      .deleteLog(this.props.logitem.id);
  }

  render() {
    return (
      <table className="LogItem">
        <thead>
          <tr>
            <th colSpan="3">
              <h2>
                <i className="fa fa-calendar-check-o" aria-hidden="true"></i>
                {dateformat(this.props.logitem.dateDiscovery, "dd/mm/yyyy")}
                &nbsp;-&nbsp;{this.props.logitem.title}</h2>
            </th>
            <th className="actionButtons"><span onClick={this._deleteLog}><i className="fa fa-trash" aria-hidden="true"></i></span></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td rowSpan="4" className="userimage"><img
              src={this.props.logitem.userimage}
              alt={"Image " + this.props.logitem.username}/><br/>
              <span className="username">{this.props.logitem.username}</span>
            </td>
          </tr>
          <tr>
            <td colSpan="3" className="description">{this.props.logitem.description}</td>
          </tr>
          <tr>
            <td className="date">Inbound:
            </td>
            <td colSpan="2" className="dateField">
              <i className="fa fa-calendar" aria-hidden="true"></i>
              {dateformat(this.props.logitem.dateStartJourney, "dd/mm/yyyy")}
            </td>
          </tr>
          <tr>
            <td className="date">
              Outbound:
            </td>
            <td colSpan="2" className="dateField">
              <i className="fa fa-calendar" aria-hidden="true"></i>
              {dateformat(this.props.logitem.dateEndJourney, "dd/mm/yyyy")}
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

/*
Prentje user
username
datum ontdekking
datum heenreis
datum terugreis
ontdekking - omschrijving
ontdekking titel
*/
export default LogItem;