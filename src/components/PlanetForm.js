import React, { Component } from 'react';
import "./planetForm.css";

class PlanetForm extends Component{
    /*constructor(props){
        super(props);

        this.state = {
            name: "No name given",
            image
        }
    }*/
    _addPlanet = (ev) => {
        if(
            this.state.name && this.state.image && this.state.icon && this.state.gravity
        ){
            this.props.addPlanet({
                name: this.state.name,
                image: this.state.image,
                icon: this.state.icon,
                gravity: this.state.gravity,
            });
        }
        ev.preventDefault();
        return false;
    }
    _changeName = (ev) => {
        this.setState({
            name: ev.target.value,
        });
    }
    _changeImage = (ev) => {
        this.setState({
            image: ev.target.value,
        });
    }
    _changeIcon = (ev) => {
        this.setState({
            icon: ev.target.value,
        });
    }
    _changeGravity = (ev) => {
        this.setState({
            gravity: ev.target.value,
        });
    }
 render(){
     return (<form className="addPlanet">
         
         <h1>Add a planet</h1>
         <table>
         <colgroup>
            <col width="100px"/>
         </colgroup>
        <tr>
            <td><label>
                Name
            </label></td>
            <td><input onChange={this._changeName} required/></td>
        </tr>
        <tr>
            <td><label>
                Image
            </label></td>
            <td><input onChange={this._changeImage} required/></td>
        </tr><tr>
            <td><label>
                Icon
            </label></td>
            <td><input onChange={this._changeIcon} required/></td>
        </tr><tr>
            <td><label>
                Gravity
            </label></td>
            <td><input onChange={this._changeGravity} required/></td>
        </tr></table>
     <input type="submit" value="Add planet" onClick={this._addPlanet} />
     <button onClick={this.props.cancel}>Cancel</button>
 </form>)
 }
}

export default PlanetForm