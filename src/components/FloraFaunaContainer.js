import React, { Component } from 'react';
import FloraFaunaItem from './FloraFaunaItem';
import FloraFaunaDetails from './FloraFaunaDetails';
import '../css/florafauna.css';

class FloraFaunaContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedItem: null,
            data: [],
            details: null,
            floraFaunaFormShown: false,
            commonName: "",
            speciesName: "",
            description: "",
            dateOfDiscovery: "",
            placeOfDiscovery: "",
            commonOccurences: "",
            image: ""
        }
    }

    componentWillMount() {
        fetch(`http://localhost:3000/faunaflora?planetId=${this.props.planetId}`).then((response) => {
            response.json().then((res) => {
                this.setState({
                    data: res
                })
            })
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.planetId !== this.props.planetId) {
            fetch(`http://localhost:3000/faunaflora?planetId=${nextProps.planetId}`).then((response) => {
                response.json().then((res) => {
                    this.setState({
                        data: res,
                        details:null
                    })
                })
            })
        }
    }

    _handleChangeCommonName = (evt) => {
        this.setState({
            commonName: evt.target.value
        })
    }

    _handleChangeSpeciesName = (evt) => {
        this.setState({
            speciesName: evt.target.value
        })
    }

    _handleChangeDescription = (evt) => {
        this.setState({
            description: evt.target.value
        })
    }

    _handleChangePlaceOfDiscovery = (evt) => {
        this.setState({
            placeOfDiscovery: evt.target.value
        })
    }

    _handleChangeDateOfDiscovery = (evt) => {
        this.setState({
            dateOfDiscovery: evt.target.value
        })
    }

    _handleChangeCommonOccurences = (evt) => {
        this.setState({
            commonOccurences: evt.target.value
        })
    }

    _handleChangeImage = (evt) => {
        this.setState({
            image: evt.target.value
        })
    }

    _changeId = (id) => {
        this.setState({
            selectedItem: this.state.data.find(item => item.id === id),

        }, () => this.setState({
            details: <FloraFaunaDetails item={this.state.selectedItem} />
        }))
    }

    _addFloraFauna = (florafauna) => {
        this.setState({
            floraFaunaFormShown: false,
        })

        let occurences = this.state.commonOccurences.split(",");

        let newflorafauna = {
            planetId: this.props.planetId,
            commonName: this.state.commonName,
            speciesName: this.state.speciesName,
            description: this.state.description,
            dateOfDiscovery: this.state.dateOfDiscovery,
            placeOfDiscovery: this.state.placeOfDiscovery,
            commonOccurences: occurences,
            properties: {},
            image: this.state.image
            
        }
        let that = this
        fetch('http://localhost:3000/faunaflora', {
            method: 'post',
            body: JSON.stringify(newflorafauna),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
          }).then(function(response) {
            response.json().then(res => {
                let newitems = [...that.state.data, res]
                that.setState({
                    data: newitems
                })
            })
          });
    }

    _showAddFloraFaunaForm = () => {
        this.setState({
            floraFaunaFormShown: true,
        })
    }
    _cancelAddFloraFauna = (ev) => {
        this.setState({
            floraFaunaFormShown: false,
        });
        ev.preventDefault();
    }

    _deleteItem = (id) => {
        let that = this
        fetch(`http://localhost:3000/faunaflora/${id}`, {
            method: 'delete',
          }).then(function(response) {

             let newItems = that.state.data.filter(x => x.id !== id) 
             that.setState({
                 data: newItems
             })
          });
    }

    render() {
        return (
            <div className="florafaunacontainer">                
                <div className="florafaunalist">
                    {this.state.data.map(item => <FloraFaunaItem deleteHandler={this._deleteItem} image={item.image}
                        name={item.commonName}
                        key={item.id}
                        itemId={item.id}
                        changeHandler={this._changeId}
                        selected={item === this.state.selectedItem ? true : false} />)}

                        <button className="addNew" onClick={this._showAddFloraFaunaForm}>Add new</button>
                </div>
                {this.state.details}      
                {this.state.floraFaunaFormShown &&
                <form>
                    <table>
                        <tbody>
                            <tr>
                                <td><label htmlFor="commonName">Common name</label></td>
                                <td><input type="text" name="commonName" onChange={this._handleChangeCommonName} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="speciesName">speciesName</label></td>
                                <td><input type="text" name="speciesName" onChange={this._handleChangeSpeciesName} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="description">description</label></td>
                                <td><input type="text" name="description" onChange={this._handleChangeDescription} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="dateOfDiscovery">dateOfDiscovery</label></td>
                                <td><input type="text" name="dateOfDiscovery" onChange={this._handleChangeDateOfDiscovery} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="placeOfDiscovery">placeOfDiscovery</label></td>
                                <td><input type="text" name="placeOfDiscovery" onChange={this._handleChangePlaceOfDiscovery} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="commonOccurences">commonOccurences</label></td>
                                <td><input type="text" name="commonOccurences" onChange={this._handleChangeCommonOccurences} required/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="image">Image url</label></td>
                                <td><input type="text" name="image" onChange={this._handleChangeImage} required/></td>
                            </tr>
                            <tr>
                                <td><button type="button" onClick={this._addFloraFauna}>Add new</button></td>
                                <td><button type="button" onClick={this._cancelAddFloraFauna}>Cancel</button></td>
                            </tr>
                        </tbody>
                    </table>                                        
                </form>}
            </div>
                    
        )    
    }
}

export default FloraFaunaContainer