import React, { Component } from 'react';


class FloraFaunaDetails extends Component {
    render() {
        let properties = this.props.item.properties;
        return (
            <div>
                <ul>
                    <li>{this.props.item.commonName}</li>
                    <li>{this.props.item.speciesName}</li>
                    <li>{this.props.item.description}</li>
                    <li>{this.props.item.dateOfDiscovery}</li>
                    <li>{this.props.item.placeOfDiscovery}</li>
                    <li>Common occurences
                        <ul>
                            {this.props.item.commonOccurences.map(el => <li key="1">{el}</li>)}
                        </ul>
                    </li>
                    {Object.keys(properties).length !== 0 &&
                    <li>Properties
                        <ul>
                            {Object.keys(properties).map(key => <li>{key}: {properties[key]}</li>)}
                        </ul>
                    </li>
                    }
                </ul>

            </div>
        )
    }
}

export default FloraFaunaDetails