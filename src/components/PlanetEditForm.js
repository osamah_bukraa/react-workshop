import React, { Component } from 'react';
import "./planetForm.css";

class PlanetForm extends Component{
    constructor(props){
        super(props);

        this.state = {
            id: this.props.planet.id,
            name: this.props.planet.name,
            image: this.props.planet.image,
            icon: this.props.planet.icon,
            gravity:this.props.planet.gravity
        }
    }
    _editPlanet = (ev) => {
        if(
            this.state.name && this.state.image && this.state.icon && this.state.gravity
        ){
            this.props.editPlanet({
                id: this.state.id,
                name: this.state.name,
                image: this.state.image,
                icon: this.state.icon,
                gravity: this.state.gravity,
            });
        }
        ev.preventDefault();
        return false;
    }
    _changeName = (ev) => {
        this.setState({
            name: ev.target.value,
        });
    }
    _changeImage = (ev) => {
        this.setState({
            image: ev.target.value,
        });
    }
    _changeIcon = (ev) => {
        this.setState({
            icon: ev.target.value,
        });
    }
    _changeGravity = (ev) => {
        this.setState({
            gravity: ev.target.value,
        });
    }
 render(){
     return (<form className="addPlanet">
         
         <h1>Edit a planet</h1>
         <table>
         <colgroup>
            <col width="100px"/>
         </colgroup>
        <tr>
            <td><label>
                Name
            </label></td>
            <td><input onChange={this._changeName} required defaultValue={this.props.planet.name}/></td>
        </tr>
        <tr>
            <td><label>
                Image
            </label></td>
            <td><input onChange={this._changeImage} required defaultValue={this.props.planet.image}/></td>
        </tr><tr>
            <td><label>
                Icon
            </label></td>
            <td><input onChange={this._changeIcon} required defaultValue={this.props.planet.icon}/></td>
        </tr><tr>
            <td><label>
                Gravity
            </label></td>
            <td><input onChange={this._changeGravity} required defaultValue={this.props.planet.gravity}/></td>
        </tr></table>
     <input type="submit" value="Save" onClick={this._editPlanet} />
     <button onClick={this.props.cancel}>Cancel</button>
 </form>)
 }
}

export default PlanetForm