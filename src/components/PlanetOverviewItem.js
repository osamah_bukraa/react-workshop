import React, { Component } from 'react';

class PlanetOverviewItem extends Component{
    _handleClick = () => {
        this.props.handleClick(this.props.planet.id);
    }

    _deletePlanet = () => {
        this.props.deletePlanet(this.props.planet.id);
    }

    _editPlanet = () => {
        this.props.editPlanet(this.props.planet);
    }

    render(){
        return(
        <div className={`planet ${this.props.selected ? "selected" : ""}`} > 
        <img 
            onClick={this._handleClick} 
            src={this.props.planet.image} 
            alt={this.props.planet.name} 
            key={this.props.planet.id}
        />
        <button onClick={this._deletePlanet} className="delete-button">x</button>
        <button onClick={this._editPlanet} className="edit"></button>
        </div>)
    }
}
export default PlanetOverviewItem