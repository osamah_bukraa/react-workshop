import React, {Component} from 'react';
import LogItem from './LogItem';
import './LogItem.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

class LogList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      inbound: moment(),
      outbound: moment(),
      discovery: moment(),
      title: "",
      description: "",
      username: "",
      picture: "assets/atomix_user31.png"
    }
    this.onchangeInbound = this
      .onchangeInbound
      .bind(this);

    this.onchangeOutbound = this
      .onchangeOutbound
      .bind(this);

    this.onchangeDiscovery = this
      .onchangeDiscovery
      .bind(this);
  }

  _ophalen(id) {
    fetch(`http://localhost:3000/logitems?planetId=${id}`)
      .then(res => res.json())
      .then(json => {
        this.setState({items: json});
      });
  }

  componentWillMount() {
    this._ophalen(this.props.planetId);
  }

  componentWillUpdate(nextProps) {
    if (this.props.planetId !== nextProps.planetId) {
      this._ophalen(nextProps.planetId);
    }
  }

  onchangeInbound = (date) => {
    this.setState({inbound: date})
  }

  onchangeOutbound = (date) => {
    this.setState({outbound: date})
  }

  onchangeDiscovery = (date) => {
    this.setState({discovery: date})
  }

  _handleChangeDescription = (ev) => {
    this.setState({description: ev.target.value})
  }

  _handleChangeTitle = (ev) => {
    this.setState({title: ev.target.value})
  }

  _handleChangeUsername = (ev) => {
    this.setState({username: ev.target.value})
  }

  _handleChangePicture = (ev) => {
    this.setState({picture: ev.target.value})
  }

  /*_addLog = () => {
    this
      .props
      .addLog({
        title: this.state.title,
        description: this.state.description,
        dateDiscovery: this.state.discovery,
        dateStartJourney: this.state.inbound,
        dateEndJourney: this.state.outbound,
        userimage: this.state.picture,
        username: this.state.username,
        planetId: this.props.planetId
      });
  }*/

  _addLog = () => {
    const newId = (+ new Date()) + "" + Math.random();
    let item = {
      id: newId, 
      title: this.state.title,
      description: this.state.description,
      dateDiscovery: this.state.discovery,
      dateStartJourney: this.state.inbound,
      dateEndJourney: this.state.outbound,
      userimage: this.state.picture,
      username: this.state.username,
      planetId: this.props.planetId
    };
    fetch("http://localhost:3000/logitems/", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: newId, 
        title: this.state.title,
        description: this.state.description,
        dateDiscovery: this.state.dateDiscovery,
        dateStartJourney: this.state.dateStartJourney,
        dateEndJourney: this.state.dateEndJourney,
        userimage: this.state.userimage,
        username: this.state.username,
        planetId: this.state.planetId
      })
    }).then(response => {
      let updatedItems = [...this.state.items, item]
      this.setState({items: updatedItems});

      this.resetFields();
    })
  }

  resetFields(){
    this.setState({
      inbound: moment(),
      outbound: moment(),
      discovery: moment(),
      title: "",
      description: "",
      username: "",
      picture: "assets/atomix_user31.png"
    });
  }

  _deleteLog = (id) => {
    fetch(`http://localhost:3000/logitems/${id}`, {method: "DELETE"}).then(response => {
      let updatedItems = this
        .state
        .items
        .filter(item => item.id !== id);
      this.setState({items: updatedItems});
    });
  }

  render() {
    return (
      <div>
        <table className="AddLogItem">
          <thead>
            <tr>
              <th colSpan="2">
                <h2>
                  <i className="fa fa-plus-circle" aria-hidden="true"></i>
                  Add new log entry</h2>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Title:</td>
              <td><input type="text" value={this.state.title} onChange={this._handleChangeTitle}/></td>
            </tr>
            <tr>
              <td>Description:</td>
              <td><input
                type="text"
                value={this.state.description}
                onChange={this._handleChangeDescription}/></td>
            </tr>
            <tr>
              <td>Date discovery:</td>
              <td><DatePicker
                onChange={this.onchangeDiscovery}
                selected={this.state.discovery}
                dateFormat="DD/MM/YYYY"/></td>
            </tr>
            <tr>
              <td>Date start journey:</td>
              <td><DatePicker
                onChange={this.onchangeInbound}
                selected={this.state.inbound}
                dateFormat="DD/MM/YYYY"/></td>
            </tr>
            <tr>
              <td>Date end journey:</td>
              <td><DatePicker
                onChange={this.onchangeOutbound}
                selected={this.state.outbound}
                dateFormat="DD/MM/YYYY"/></td>
            </tr>
            <tr>
              <td>Username:</td>
              <td><input
                type="text"
                value={this.state.username}
                onChange={this._handleChangeUsername}/></td>
            </tr>
            <tr>
              <td>
                
              </td>
              <td>
                <button onClick={this._addLog}>Submit</button>
              </td>
            </tr>
          </tbody>
        </table>
        {this
          .state
          .items
          .map(item => {
            return <LogItem logitem={item} key={item.id} deleteLog={this._deleteLog}/>
          })}
      </div>
    );
  }
}

export default LogList;