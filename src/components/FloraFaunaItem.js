import React, { Component } from 'react';

class FloraFaunaItem extends Component {
    _selectItem = () => {
        this.props.changeHandler(this.props.itemId)
    }

    _deleteItem = () => {
        this.props.deleteHandler(this.props.itemId)
    }

    render() {
        return (
            <div className={this.props.selected ? "active-florafauna florafaunaitem" : "florafaunaitem"}>
                
                <figure onClick={this._selectItem}>
                    <img src={this.props.image} alt={this.props.name} />
                    <figcaption>{this.props.name}</figcaption>
                </figure>
                <button className="delete-button" onClick={this._deleteItem}>x</button>
            </div>
        )
    }
}

export default FloraFaunaItem