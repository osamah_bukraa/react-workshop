import React, {Component} from 'react';
import './App.css';
import PlanetOverview from "./components/PlanetOverview";
import DetailsPage from "./components/DetailsPage";
import LogList from "./components/LogList";
import FloraFaunaContainer from './components/FloraFaunaContainer';
import CommentList from './components/CommentList';
import './font-awesome/css/font-awesome.min.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      planets: [],
      selectedPlanet: null,
      logitems: []
    };
  }
  componentWillMount() {
    fetch("http://localhost:3000/planets").then(response => {
      response
        .json()
        .then(planets => this.setState({planets: planets}));
    });
  }
  _addPlanet = (planet) => {
    const newId = (+ new Date()) + "" + Math.random();
    const planets = this.state.planets;
    fetch("http://localhost:3000/planets/", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id: newId, name: planet.name, gravity: planet.gravity, icon: planet.icon, image: planet.image})
    }).then(response => {

      planet.id = newId;
      planets.push(planet);
      this.setState({planets: planets});
    })
  }

  _deletePlanet = (id) => {
    fetch(`http://localhost:3000/planets/${id}`, {method: "DELETE"}).then(response => {
      const planets = this
        .state
        .planets
        .filter(planet => planet.id !== id);
      this.setState({planets: planets});
    });
  }

  _editPlanet = (planet) =>{
    const planets = this.state.planets;
    fetch(`http://localhost:3000/planets/${planet.id}`, {
      method: "PUT", 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(planet)
    }).then(response => {
      const index = planets.findIndex(p => p.id === planet.id);
      planets[index] = planet;
      this.setState({
        planets: planets
      });
    })
  }

  _changePlanet = (id) => {
    console.log(id, this);
    this.setState({
      selectedPlanet: this
        .state
        .planets
        .filter(planet => planet.id === id)[0]
    })
  }

  _addComment = (comment) => {
    this.setState(prevState => ({
      comments: prevState
        .comments
        .concat(comment)
    }));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <PlanetOverview
            selectedPlanet={this.state.selectedPlanet}
            planets={this.state.planets}
            changePlanet={this._changePlanet}
            addPlanet={this._addPlanet}
            deletePlanet={this._deletePlanet}
            editPlanet = {this._editPlanet}
          />
        </header>
        {this.state.selectedPlanet
          ? (
            <div>
              <DetailsPage planet={this.state.selectedPlanet}/>
              <CommentList
                handleAddComment={this._addComment}
                planetId={this.state.selectedPlanet.id}/>
              <LogList
                planetId={this.state.selectedPlanet.id}/>
              <FloraFaunaContainer planetId={this.state.selectedPlanet.id}/>
            </div>
          )
          : <p>No planet selected!</p>}

      </div>
    );
  }
}

export default App;
